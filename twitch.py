#!/usr/bin/env python3

import curses
import curses.ascii
import curses.textpad
import requests
import requests_futures.sessions
import json
import copy
import subprocess
import sys
import threading

FOCUS_STATUS = 0
FOCUS_BROWSE = 1

PAIR_SEARCH_ACTIVE = 1
PAIR_SEARCH_INACTIVE = 2
PAIR_HILIGHT = 3
PAIR_DEFAULT_TEXT = 4

GET_VIDEOS_PAYLOAD = [
    {
        "operationName": "FilterableVideoTower_Videos",
        "variables": {
            "limit": 30,
            "channelOwnerLogin": "",
            "broadcastType": None,
            "videoSort": "TIME"
            },
        "extensions": {
            "persistedQuery": {
                "version": 1,
                "sha256Hash": "186f0f65e4db53453e5744ed43b3e1605b2529ebc1e816b97306b143d695eb79"
                }
            }
        }
    ]

class BrowseItem:
    def __init__(self, name, url):
        self.name = name
        self.url = url

def tput(cap, *args):
    tb = curses.tigetstr(cap)
    if tb is not None:
        if len(args) > 0:
            tb = curses.tparm(tb, *args)
        print(tb.decode('ASCII'))

def main(stdscr):
    curses.raw()
    curses.use_default_colors()
    #curses.init_color(curses.COLOR_BLUE, 0, 0, 950)
    curses.init_pair(PAIR_SEARCH_ACTIVE, curses.COLOR_WHITE, curses.COLOR_BLUE);
    try:
        curses.init_pair(PAIR_SEARCH_INACTIVE, curses.COLOR_WHITE, curses.COLOR_BLUE+8);
    except curses.error:
        curses.init_pair(PAIR_SEARCH_INACTIVE, curses.COLOR_WHITE, curses.COLOR_MAGENTA);
    curses.init_pair(PAIR_HILIGHT, curses.COLOR_BLACK, curses.COLOR_WHITE);
    curses.init_pair(PAIR_DEFAULT_TEXT, -1, -1);
    scy, scx = stdscr.getmaxyx()

    browsewin = curses.newwin(scy - 1, scx, 0, 0)
    browsewin.attron(curses.color_pair(PAIR_DEFAULT_TEXT))
    browsewin.leaveok(True)

    statusbar = curses.newwin(1, scx, scy - 1, 0)
    statusbar.attron(curses.color_pair(PAIR_SEARCH_ACTIVE))
    statusbar.bkgdset(0, curses.color_pair(PAIR_SEARCH_ACTIVE))

    stdscr.refresh()
    stdscr.timeout(10)

    focus = FOCUS_STATUS
    search_text = ''
    search_cursor = 0
    search_scroll = 0
    browse_items = []
    browse_cursor = -1
    browse_scroll = 0
    status_dirty = True
    browse_dirty = False
    browse_last_cursor = None
    current_search = None
    loading_text = None
    completed_responses = [None]
    video_load_lock = threading.Lock()
    key = curses.KEY_RESIZE
    placeholder_text = ''
    lastkey = -1

    session = None

    def videos_received(sess, resp):
        with video_load_lock:
            completed_responses[0] = resp


    while True:
        with video_load_lock:
            response = completed_responses[0]
            if response is not None:
                completed_responses[0] = None
                loading_text = None
                try:
                    response.raise_for_status()
                except requests.HTTPError as e:
                    loading_text = str(e)
                    try:
                        loading_text += '\n' + json.dumps(response.json(), indent=2)
                    except json.JSONDecodeError:
                        loading_text += '\n' + response.text()
                    browse_dirty = True
                if loading_text is None:
                    rdata = response.json()
                    browse_items = []
                    user = rdata[0]["data"]["user"]
                    if user is None:
                        loading_text = 'No such user: {}'.format(current_search)
                    else:
                        videos = user["videos"]
                        if videos is not None:
                            for edge in videos["edges"]:
                                video = edge["node"]
                                d = video["lengthSeconds"]
                                if d >= 60 * 60:
                                    duration = '{}:{:0>2}:{:0>2}'.format(d // (60 * 60), d % (60 * 60) // 60, d % 60)
                                elif d >= 60:
                                    duration = '{}:{:0>2}'.format(d // 60, d % 60)
                                else:
                                    duration = d
                                    
                                if video["game"]:
                                    name = '{} [{}] ({})'.format(video["title"], video["game"]["displayName"], duration)
                                else:
                                    name = '{} ({})'.format(video["title"], duration)
                                url = 'https://twitch.tv/videos/{}'.format(video["id"])
                                browse_items.append(BrowseItem(name, url))
                    if len(browse_items) > 0:
                        browse_cursor = 0
                        browse_last_cursor = -1
                    else:
                        browse_cursor = -1
                    browse_dirty = True
                    browse_scroll = 0

        if key == curses.KEY_RESIZE or lastkey == curses.KEY_RESIZE:
            scy, scx = stdscr.getmaxyx()

            browsewin.resize(scy - 1, scx)
            browse_dirty = True

            statusbar.mvwin(scy - 1, 0)
            statusbar.resize(1, scx)
            status_dirty = True

        elif key == curses.ascii.ETX:
            return
        elif focus == FOCUS_STATUS:
            if key == curses.ascii.LF and len(search_text) > 0:
                postdata = copy.deepcopy(GET_VIDEOS_PAYLOAD)
                postdata[0]["variables"]["channelOwnerLogin"] = search_text
                if session is not None:
                    session.close()
                session = requests_futures.sessions.FuturesSession()
                current_request = session.post('https://gql.twitch.tv/gql',
                    data=json.dumps(postdata),
                    headers={'client-id': 'kimne78kx3ncx6brgo4mv6wki5h1ko'},
                    background_callback=videos_received)
                loading_text = 'Loading videos for {}...'.format(search_text)
                current_search = search_text
                browse_scroll = 0
                search_cursor = 0
                search_scroll = 0
                search_text = ''
                status_dirty = True
                browse_dirty = True
                focus = FOCUS_BROWSE
            elif key == curses.ascii.BEL:
                status_dirty = True
                browse_dirty = True
                focus = FOCUS_BROWSE
            elif lastkey == curses.ascii.ESC and key == ord('b'):
                if search_cursor > 0:
                    search_cursor -= 1
                while search_cursor > 0 and curses.ascii.isspace(search_text[search_cursor]):
                    search_cursor -= 1
                while search_cursor > 0 and not curses.ascii.isspace(search_text[search_cursor]):
                    search_cursor -= 1
                if search_cursor > 0:
                    search_cursor += 1
                if search_cursor < search_scroll:
                    search_scroll = search_cursor
                status_dirty = True
            elif lastkey == curses.ascii.ESC and key == ord('f'):
                if search_cursor < len(search_text):
                    search_cursor += 1
                orig = search_cursor
                while search_cursor < len(search_text) and curses.ascii.isspace(search_text[search_cursor]):
                    search_cursor += 1
                while search_cursor < len(search_text) and not curses.ascii.isspace(search_text[search_cursor]):
                    search_cursor += 1
                if search_cursor != orig:
                    search_cursor -= 1
                if search_cursor >= scx:
                    search_scroll = search_cursor - scx + 1
                status_dirty = True
            elif curses.ascii.isprint(key):
                search_text = search_text[0:search_cursor] + chr(key) + search_text[search_cursor:]
                search_cursor += 1
                if search_cursor >= scx:
                    search_scroll = search_cursor - scx + 1
                status_dirty = True
            elif key == curses.ascii.SOH:
                search_cursor = 0
                search_scroll = 0
                status_dirty = True
            elif key == curses.ascii.ENQ:
                search_cursor = len(search_text)
                if search_cursor >= search_scroll + scx:
                    search_scroll = search_cursor - scx + 1
                status_dirty = True
            elif key == curses.ascii.VT:
                search_text = search_text[0:search_cursor]
                status_dirty = True
            elif key in (curses.KEY_DC, curses.ascii.EOT) and search_cursor < len(search_text):
                search_text = search_text[0:search_cursor] + search_text[search_cursor + 1:]
                status_dirty = True
            elif key in (curses.ascii.STX,curses.KEY_LEFT, curses.ascii.DEL,curses.KEY_BACKSPACE) and search_cursor > 0:
                if key in (curses.ascii.DEL,curses.KEY_BACKSPACE):
                    search_text = search_text[0:search_cursor - 1] + search_text[search_cursor:]
                search_cursor -= 1
                if search_cursor < search_scroll:
                    search_scroll = search_cursor
                status_dirty = True
            elif key in (curses.ascii.ACK, curses.KEY_RIGHT) and search_cursor < len(search_text):
                search_cursor += 1
                if search_cursor >= search_scroll + scx:
                    search_scroll = search_cursor - scx + 1
                status_dirty = True

        elif focus == FOCUS_BROWSE:
            if browse_cursor != -1:
                if key == curses.KEY_UP and browse_cursor > 0:
                    browse_last_cursor = browse_cursor
                    browse_cursor -= 1
                    if browse_cursor < browse_scroll:
                        browse_scroll = browse_cursor
                        browse_dirty = True
                elif key == curses.KEY_DOWN and browse_cursor < len(browse_items) - 1:
                    browse_last_cursor = browse_cursor
                    browse_cursor += 1
                    if browse_cursor >= browse_scroll + scy - 1:
                        browse_scroll = browse_cursor - scy + 2
                        browse_dirty = True
                elif key == curses.KEY_PPAGE and browse_cursor > 0:
                    if browse_cursor != browse_scroll:
                        browse_last_cursor = browse_cursor
                        browse_cursor = browse_scroll
                    else:
                        browse_cursor = max(0, browse_scroll - scy + 1)
                    if browse_cursor < browse_scroll:
                        browse_scroll = browse_cursor
                        browse_dirty = True
                elif key == curses.KEY_NPAGE and browse_cursor < len(browse_items) - 1:
                    if browse_cursor != browse_scroll + scy - 1:
                        browse_last_cursor = browse_cursor
                        browse_cursor = min(len(browse_items) - 1, browse_cursor + scy - 2)
                    else:
                        browse_cursor = min(len(browse_items) - 1, browse_cursor + scy - 1)
                    if browse_cursor >= browse_scroll + scy - 1:
                        browse_scroll = browse_cursor - scy + 2
                    browse_dirty = True
                elif key == curses.KEY_HOME and browse_cursor > 0:
                    browse_cursor = 0
                    browse_scroll = 0
                    browse_dirty = True
                elif key == curses.KEY_END and browse_cursor < len(browse_items) - 1:
                    browse_cursor = len(browse_items) - 1
                    if browse_cursor >= browse_scroll + scy - 1:
                        browse_scroll = browse_cursor - scy + 2
                    browse_dirty = True
                elif key == curses.ascii.LF:
                    url = browse_items[browse_cursor].url
                    #subprocess.Popen(['mpv', url], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                    stdscr.clear()
                    stdscr.refresh()
                    curses.nocbreak()
                    curses.echo()
                    curses.endwin()
                    tput('clear')
                    subprocess.run(['mpv', url])
                    placeholder_text = 'Opening {}...'.format(url)
                    stdscr = curses.initscr()
                    curses.noecho()
                    curses.raw()
                    browse_dirty = True
                    status_dirty = True
            if key == curses.ascii.BEL:
                status_dirty = True
                focus = FOCUS_STATUS

        if status_dirty or browse_dirty:
            if focus == FOCUS_STATUS:
                curses.curs_set(1)
            else:
                curses.curs_set(0)

        if browse_dirty:
            if len(placeholder_text) > 0:
                placeholder_text = ''
                status_dirty = True
            browsewin.clear()
            if loading_text is not None:
                loading_lines = loading_text.split('\n')
                for i in range(min(len(loading_lines), scy - 1)):
                    browsewin.move(i, 0)
                    browsewin.addstr(loading_lines[i][0:scx - 1])
            else:
                item_count = len(browse_items)
                if item_count > 0:
                    for i in range(browse_scroll, min(browse_scroll + scy - 1, item_count)):
                        name = browse_items[i].name.replace('\n','')
                        browsewin.move(i - browse_scroll, 0)
                        attr = curses.color_pair(PAIR_HILIGHT) if i == browse_cursor else 0
                        #import pdb; pdb.set_trace()
                        linestr = name[0:scx - 1] + ' ' * (scx - len(name) - 1)
                        browsewin.addstr(linestr, attr)
            browsewin.refresh()
        if browse_last_cursor is not None:
            if len(placeholder_text) > 0:
                placeholder_text = ''
                status_dirty = True
            if browse_last_cursor != -1:
                name = browse_items[browse_last_cursor].name
                browsewin.move(browse_last_cursor - browse_scroll, 0)
                browsewin.addstr(name[0:scx - 1] + ' ' * (scx - len(name) - 1))
            name = browse_items[browse_cursor].name
            browsewin.move(browse_cursor - browse_scroll, 0)
            browsewin.addstr(name[0:scx - 1] + ' ' * (scx - len(name) - 1), curses.color_pair(PAIR_HILIGHT))
            browsewin.refresh()

        if status_dirty:
            statusbar.clear()
            if focus == FOCUS_STATUS:
                statusbar.attron(curses.color_pair(PAIR_SEARCH_ACTIVE))
                statusbar.bkgd(0, curses.color_pair(PAIR_SEARCH_ACTIVE))
            else:
                statusbar.attron(curses.color_pair(PAIR_SEARCH_INACTIVE))
                statusbar.bkgd(0, curses.color_pair(PAIR_SEARCH_INACTIVE))

            statusbar.move(0, 0)
            if focus != FOCUS_STATUS and len(placeholder_text) > 0:
                statusbar.addstr(placeholder_text[0:scx - 1])
            elif focus == FOCUS_STATUS and len(search_text) == 0:
                text = 'Search for a user...'
                statusbar.addstr(text[0:scx - 1])
                statusbar.move(0, 0)
            else:
                statusbar.addstr(search_text[search_scroll:search_scroll+scx - 1])
                statusbar.move(0, search_cursor - search_scroll)
            statusbar.refresh()

        status_dirty = False
        browse_dirty = False
        browse_last_cursor = None

        lastkey = key
        try:
            key = stdscr.getch()
        except curses.error:
            key = -1

curses.wrapper(main)
tput('clear')
